## Суулах хэрэгтэй доорх хэдийг :)

- [vifm](https://github.com/vifm/vifm) суулгах `sudo apt install vifm` гээд болчих байхаа
- vifm-ээр зураг,pdf preview хийж харах учраас [ueberzug](https://github.com/seebye/ueberzug) суулгана.
  > pip install ueberzug эсвэл pip3 install ueberzug гээд болчих байхаа.
- vfim-ээр бичлэгний preview үзэх тул бас ffmpeg суулгах хэрэгтэй.
  > sudo apt install ffmpeg sudo apt install ffmpeg
- [picom](https://github.com/ibhagwan/picom) болон [bspwm](https://github.com/baskerville/bspwm) бас sxhkd суулгана дээр дараад зааврыг харж болно. Эсвэл [энэ](https://www.youtube.com/watch?v=uucr0xHoz1A&t=556s) бичлэг үзээд хийж болно.
  > Бичлэг өөр олон юм суулгаж байгаа зөвхөн дээрх хэдийг суулгаж байгааг хараад хийгээрэй.
- [polybar-collection](https://github.com/adi1090x/polybar-themes) суулгана.
